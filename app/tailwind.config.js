/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./assets/**/*.js",
    "./templates/**/*.html.twig",
  ],
  theme: {
    extend: {
      fontFamily: {
        'nixie': ["Nixie One", 'sans-serif'],
        'barlow': ["Barlow", 'sans-serif']
      },
    },
  },
  plugins: [require("daisyui")],
}
