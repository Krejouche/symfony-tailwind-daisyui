#---VARIABLES---------------------------------#
#---DOCKER---#
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_COMPOSE = $(DOCKER) compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop
DOCKER_EXEC = $(DOCKER) exec -it
#------------#

#---CONTAINER---#
CONTAINER_PHP = sf_tw_daisy_php
#---------------#

#---SYMFONY---#
PHP_BIN_CONSOLE = php bin/console
SYMFONY = symfony
SYMFONY_CONSOLE = $(SYMFONY) console
#------------#


## === 🆘  HELP ==================================================
help: ## Show this help.
	@echo "Symfony-And-Docker-Makefile"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
#----------------------------------------------------------------#

## === 🚀  PROJECT ==================================================
install: ## Install the project
	@make stop
	cd docker && $(DOCKER_COMPOSE) up -d && cd ..
	@make composer_install
	# @make fixtures_load

install_build: ## Install the project
	@make stop
	cd docker && $(DOCKER_COMPOSE) up -d --build && cd ..
	@make composer_install
	# @make fixtures_load

stop: ## Stop the project
	$(DOCKER_COMPOSE) --file 'docker/compose.yaml' --project-name 'juju' stop

bash_php: ## Open a bash in the php container
	$(DOCKER_EXEC) $(CONTAINER_PHP) bash

composer_install: ## Install the composer dependencies
	$(DOCKER_EXEC) $(CONTAINER_PHP) composer install
#----------------------------------------------------------------#

## === DEV ==================================================
watch: ## Watch the assets
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) tailwind:build --watch

build: ## Build the assets before deploying
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) tailwind:build --minify
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) asset-map:compile
#----------------------------------------------------------------#


## === 🎛️  SYMFONY ==================================================
cc: ## Clear the cache
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) cache:clear

debug_router: ## Debug the routes
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) debug:router

debug_container: ## Debug the container
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) debug:container

user_entity: ## Create a new user entity
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) make:user

registration_form: ## Create a new registration form
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) make:registration-form

entity: ## Create a new entity
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) make:entity

db_create: ## Create the database
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) doctrine:database:create

db_drop: ## Drop the database
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) doctrine:database:drop --force

migration: ## Create a new migration
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) make:migration

migrate: ## Execute the migrations
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) doctrine:migrations:migrate --no-interaction

fixtures_load: ## Load the fixtures
	@make db_drop
	@make db_create
	@make migrate
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) doctrine:fixtures:load --no-interaction

controller: ## Create a new controller
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) make:controller
#----------------------------------------------------------------#

## === 📋  EasyAdmin ==================================================
dashboard: ## Create a new dashboard
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) make:admin:dashboard

admin_crud: ## Create a new CRUD controller
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) make:admin:crud
#----------------------------------------------------------------#

## === 🧪  TEST ==================================================
run_test: ## Run the tests
	$(DOCKER_EXEC) $(CONTAINER_PHP) php bin/phpunit --testdox

test: ## Create unit tests
	$(DOCKER_EXEC) $(CONTAINER_PHP) $(PHP_BIN_CONSOLE) make:test
#----------------------------------------------------------------#

## === 🐛  Quality =================================================
quality: ## Run the quality tools to test coding standards
	@make syntax
	@make phpmd
	@make phpstan

syntax: ## Run PHPCBF & PHPCS tools
	docker run -it -v ${PWD}/app:/app -w /app jakzal/phpqa:php8.3 phpcbf  --standard=PSR12  --extensions=php /app/src/ || true
	docker run -it -v ${PWD}/app:/app -w /app jakzal/phpqa:php8.3 phpcs  --standard=PSR12  --extensions=php /app/src/ || true

phpmd: ## Run the PHPMD tool
	docker run -it -v ${PWD}/app/src:/src -v ${PWD}/quality_control/phpmd:/ruleset -w /app jakzal/phpqa:php8.3 phpmd /src ansi /ruleset/ruleset.xml

phpstan: ## Run the PHPStan tool
	docker run -it -v ${PWD}/app:/app -w /app jakzal/phpqa:php8.3 phpstan analyse --level 9 src/
#----------------------------------------------------------------#
