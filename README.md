# Dockerized Symfony 7 with TailwindCSS and DaisyUI on asset mapper

It's really all in the title: a Symfony 7 project with TailwindCSS and DaisyUI, all dockerized and ready to go.

Included in the project:

- **Makefile** with a few commands to help you get started
- **Gitlab CI** witn a few code checks including coding standards (PHPStan, PHP-CS, PHPMD) & PHPUnit test ready
- **Docker env**:
  - Compose
  - Composer
  - NPM
- **Source code**:
  - Symfony 7
  - Asset mapper
  - TailwindCSS
  - DaisyUI

## Installation

1. Clone the repository
2. cd into the project directory
3. Run `make install_biomd` to install the project
4. Run `make bash_php` to enter the PHP container
5. Run `composer install` to install the PHP dependencies
6. Run `npm install` to install the JS and CSS dependencies
7. Exit the container
8. Run `make watch` to start the asset mapper
9. Open your browser and go to `http://localhost/test` to see the project test controller in action
10. Start coding!
